\documentclass{article}

\usepackage{amsmath,titling,graphicx,amssymb}

\newcommand{\V}[1]{\mathbf{#1}}
\newcommand{\spikes}[2]{S_{#1,#2}}
\newcommand{\mean}[1]{\left<#1\right>}
\newcommand{\f}[0]{r(t;\lambda_1,\lambda_2)}
\newcommand{\pair}{(\lambda_1,\lambda_2)}
\newcommand{\fixedpair}{(\lambda_1^*,\lambda_2^*)}
\newcommand{\E}[1]{\mathbb{E}\left[#1\right]}

\begin{document}

\title{Bayes rule links neural membranes \\ with Poisson likelihoods}
\author{}
\date{\vspace{-5ex}}
\maketitle



\abstract

%According to the Bayesian brain hypothesis, nervous systems implement Bayesian inference given uncertain sensory data.  Animal reaction times indicate that nervous systems compute on the order of milliseconds.  Establishing whether neurons represent probability distributions, and how they implement inference so fast, are major open questions in neuroscience.  We analytically show that an impulse-driven RC circuit infers the firing intensity of a Poisson-spiking afferent exactly and online.  Thus, we derive the classic Ôleaky integratorÕ model of a neuron directly from principles of probability theory.  The leaky integrator's time constant and synaptic weight have intuitive interpretations in a Bayesian context.  Sentence under construction.  Neurons might be high-precision devices that perform exact, analog, online Bayesian inference.  Another implication.

\section{Introduction}

%Animals behave as if their perceptions, decisions, and actions are Bayesian.
%
%Bayesian inference offers a normative framework that explains why nervous systems do what they do.  The key questions are %how.  Cite literature here.  Our focus is on real-time implementation.
%
%The relationship between inference of a Poisson process and RC circuits are conspicuously absent from the literature (novel).  %Cite papers that hone in on first-order differential equations (including Deneve and Mate Lengyel, and plenty others).  Then %models of neurons as RC circuits (Koch, citations, etc).  But the simple link between them has not been made explicit.  
%
%That link happens to be Bayesian.  One of the benefits of this link is that we can work directly with probabilities, and not dip into %log space (Deneve).  Other benefits over Deneve.  This is NOT DENEVE'S WORK.
%
%Likelihoods underpin all of statistics. The classical decision/model selection/inference criterion is likelihood ratio.  Odds ratio is a posterior probability ratio, which is better, but still only for binary choices.  Log odds ratio is even better, for numerical precision/stability reasons.  You can do maximum likelihood (=max likelihood ratio wrt all other choices) without normalization, which is max posterior probability  with a uniform prior. But the likelihood (ratio) computers need to be calibrated.  Calibrating neurons (so that they all compute likelihood on the same scale) would seem to be a harder  problem than normalization.  
%
%A single neuronal membrane patch computes Poisson likelihood. This is a fact, independent of whether it's relevant to understanding neural computation.  If it is relevant to neural computation then it could be applied in many different ways (corresponding to the diverse applications of likelihood in inference and decision theory, frequentist and Bayesian).  
%
%Deneve's work:
%Dealing with output spikes.  It's important because on some level you need a hierarchy.  And as Deneve says, if you want to perform further probabilistic computations, the output spikes are all you have to work with.
%
%Predictive coding:  We do not address this issue.  
%Time constant interpretations are different.
%She needs one free parameter.  We need none.
%She deals with log-odds.  We deal with probability ratio.  Check her reference though; she %mentions people who calculate the continuous posterior ratio.  See how they compare as well.
%
%Cell membranes can infer Poisson intensities.  This is from a trivial mathematical relationship between Poisson likelihoods and first-order passive circuits.

\section{Results}

\subsection{Leaky integrators infer Poisson intensity \\ ratios exactly and online}

\label{LI}

Consider an afferent neuron that generates Poisson-distributed spikes with a constant intensity $\lambda$.  We do not know $\lambda$, and we wish to infer it in real-time as spikes $S$ are observed or not.

By Bayes' rule:
%
\begin{equation*}
 \Pr(\lambda|S) = \Pr(S|\lambda)\Pr(\lambda)/\Pr(S),
\end{equation*}
%
where $\Pr(S|\lambda)$ is the likelihood and $\Pr(\lambda)$ is an assumed prior belief of $\lambda$.  We can eliminate the normalization constant $\Pr(S)$ by considering a ratio of probabilities of two particular intensities $\lambda_1, \lambda_2$:
%
\begin{equation*}
\frac{\Pr(\lambda_1|S)}{\Pr(\lambda_2|S)} = \frac{\Pr(S|\lambda_1)\Pr(\lambda_1)}{\Pr(S|\lambda_2)\Pr(\lambda_2)}.
\end{equation*}
%
Say $k$ spikes have been observed in time $t$.  Inserting Poisson distributions for the likelihoods:
%
\begin{equation*}
\frac{\Pr(\lambda_1|S=k)}{\Pr(\lambda_2|S=k)} = \left( \frac{\lambda_1}{\lambda_2} \right) ^ k e^{(\lambda_2 - \lambda_1)t} \frac{\Pr(\lambda_1)}{\Pr(\lambda_2)} \equiv \f.
\end{equation*}
%
The dependence of $r$ on $t$ and $k$ specify how it should change, in real-time, as spikes are observed or not (see Figure 1).

\begin{figure}[ht!]
\begin{center}
% Use the relevant command to insert your figure file.
% For example, with the graphicx package use
  \includegraphics[width=12cm]{figure1.pdf}
\end{center}
% figure caption is below the figure
\caption{\scriptsize A leaky integrator calculates one specific ratio of the intensity's posterior in real-time.  Left, top:  Observed afferent spiking output $S$.  We simulated a Poisson process with intensity $\lambda=10$ for 1000 time steps of duration $dt = 0.0001$.  One spike occurred at time step 651.  Left, bottom:  Various snapshots of $\Pr(\lambda|S)$ as the afferent spiking output is observed in time.  We assumed a uniform prior (trace 0) over $\lambda \in [0,100]$ ($\lambda$ only shown up to 65).   As no spikes are observed (traces 250, 500), the posterior decays to the left, as lower values of $\lambda$ become more likely.  At time step 651, when a spike is observed, the posterior jumps to the right, as higher values of $\lambda$ become more likely (trace 651).  As no spikes are observed, the posterior decays to the left again (traces 850, 1000).  Right:  Ratios of the posterior between various points $\lambda_1, \lambda_2, \lambda_3$ and $\lambda_4$ as marked in the left panel.  Ratios are plotted in time with thick gray lines.  The thin black lines (labeled RC) are the voltage traces of three leaky integrators with time constants $\tau_{12} = 1/(\lambda_1-\lambda_2)$, $\tau_{13} = 1/(\lambda_1-\lambda_3)$, $\tau_{14} = 1/(\lambda_1-\lambda_4)$, and synaptic weights $w_{12} = \lambda_1/\lambda_2$, $w_{13}=\lambda_1/\lambda_3$, $w_{14} = \lambda_1/\lambda_4$.  A leaky integrator (Eqs.\ \ref{leaky}, \ref{realleaky}) with a particular time constant and synaptic weight computes one specific posterior ratio exactly and in real-time.}
\label{bigpicture}       % Give a unique label
\end{figure}

As no spikes are observed (i.e.\ as $k$ is held constant), $r$ is a solution to first-order differential equations of the form:
%
\begin{equation*}
\dot{r}(t) + (\lambda_1 - \lambda_2) r(t) = 0.
\end{equation*}
%
The natural response of a series RC circuit is:
%
\begin{equation*}
\dot{V}(t) + \frac{1}{RC}V(t) = 0.
\end{equation*} 
%
If the circuit's initial condition is:
%
\begin{equation*}
V(0) = \Pr(\lambda_1)/\Pr(\lambda_2),
\end{equation*}
%
then the ratio of the posterior at two points $\pair$ is the natural response of an RC circuit with time constant $\tau = RC = 1/(\lambda_1 - \lambda_2)$, as no spikes are observed (see Figure 1).

When a spike is observed, $r$ is instantaneously modified by an additional factor $\lambda_1/\lambda_2$.  Letting the spike be observed at time $t_1$:
%
\begin{equation*}
r(t_1^+) = \frac{\lambda_1}{\lambda_2} r(t_1^-),
\end{equation*}
%
in C\`adl\`ag notation.  So if the circuit's voltage at $t_1$ is set to:
%
\begin{equation*}
V(t_1^+) = \frac{\lambda_1}{\lambda_2} V(t_1^-),
\end{equation*}
%
then the circuit's voltage equals the posterior's ratio at two points, given the observation of the spike (see Figure 1).

The RC circuit's voltage is set in this manner if it is driven by voltage $ V(t_1) \lambda_1  / \lambda_2$ when a spike arrives:
%
\begin{equation}
\label{leaky}
\dot{V} + (\lambda_1 - \lambda_2)V = \frac{\lambda_1}{\lambda_2} V \sum_{t_j < t} \delta(t - t_j),
\end{equation}
%
where $t_j$ represents the $j^\text{th}$ spike time, and $\delta$ is the Kronecker delta.  

Eq.\ \ref{leaky} has the same form as the leaky integrator (LI) (refs):
%
\begin{equation}
\label{realleaky} 
\dot{V} + \frac{1}{RC}V = w V \sum_{t_j<t} \delta(t-t_j),
\end{equation}
%
where we have set the leak voltage to 0 without loss of generality.  

The LI calculates the ratio of two posterior intensities of a Poisson-spiking afferent exactly and in real-time.

\subsection{Thresholded LIs implement sequential probability ratio tests}

\label{Wald}

Sequential probability ratio tests (SPRT) accept one of two statistical hypotheses based on the likelihood ratio of observations.  They employ two thresholds to accept either hypothesis.  It follows that a thresholded LI implements a SPRT for two particular Poisson intensities.

In discrete time, a LI's voltage represents the quantity:
%
\begin{equation*}
V_t = \frac{\Pr(\lambda_1|S=k)}{\Pr(\lambda_2|S=k)} = \frac{\Pr(\lambda_1)}{\Pr(\lambda_2)}\prod_{i=1}^t \frac{\Pr(S_i|\lambda_1)}{\Pr(S_i|\lambda_2)},
\end{equation*}
%
where $S_i = 0 \text{ or } 1$ represents the afferent's spiking output on a sufficiently small time step, and $t$ is the number of time steps. 

For simplicity we assume that the ratio of priors is 1 (i.e.\ a uniform prior), but the following analysis can be extended if that assumption does not hold.  Also representing the likelihood ratio as $L_i$:
%
\begin{equation*}
V_t = \prod_{i=1}^t L_i.
\end{equation*}

Let $T$ be the (random) time step where $V_T$ exceeds one of two thresholds $0<a<1<b<\infty$.  If $V_T > b$ then the LI accepts $\lambda_1$, and if $V_T < a$ then the LI accepts $\lambda_2$.  

Wald (refs) formulated SPRTs as a sequential analysis problem.  In this formulation, we can analytically obtain the probability that the LI will hit either threshold first, i.e.\ accept either intensity.  We can also obtain the (conditional and marginal) distributions of the LI's time to acceptance.  We present Wald's analysis in the Appendix.  

Figure \ref{SPRT} shows example conditional and marginal threshold crossing time distributions obtained from his analysis.  Say the LI spikes and its voltage is reset to 1 after it accepts an intensity, and it repeats the SPRT.  Then the distributions to acceptance time can be interpreted as the distribution of the LI's interspike intervals.  Furthermore, we can analyze how the LI's interspike interval distributions depend on its biophysical parameters.

\begin{figure}[ht!]
\begin{center}
% Use the relevant command to insert your figure file.
% For example, with the graphicx package use
  \includegraphics[width=12cm]{SPRT.pdf}
\end{center}
% figure caption is below the figure
\caption{\scriptsize A thresholded LI performs a SPRT on two particular Poisson intensities.  Wald's analysis yields the probability that the LI will accept either intensity, and the (conditional and marginal) distributions of the time required for it to do so.  Main plot:  8 example LI voltage traces, each starting at 1, with black dots marking their threshold crossings.  Solid black traces near each threshold $b$ and $a$ are the distributions of time required for the LI to hit that threshold, obtained from the conditional characteristic functions (see Appendix).  Histograms are the results of 100000 simulations.  Upper inset:  Solid black trace is the marginal distribution of time required for the LI to hit a threshold, with conditional time distributions replotted as dashed black traces.  Histogram shows simulation results.  If we reset the LI's voltage when it crosses a threshold and repeat the SPRT, then this marginal distribution is the LI's interspike interval distribution.  Lower inset:  Probabilities that the LI will accept either intensity, and the percentage of acceptances observed in simulations.  Simulations fit Wald's analysis very well, but not exactly, due to overshoots of threshold $b$.  When the overshoot is negligible compared to the distance between the thresholds, the fit becomes exact.  For all plots, $\lambda_1 = 45$ Hz, $\lambda_2 = 40$ Hz, and the true afferent spiking intensity was $\lambda = 40$ Hz.  The time step duration $\Delta t = .001$s, and the units of the horizontal axes are the number of time steps.  The thresholds $b = 1.5$ and $a = .3$.}
\label{SPRT}       % Give a unique label
\end{figure}

\subsection{Probabilistic interpretation of biophysical parameters}

Comparing Eqs.\ \ref{leaky} and \ref{realleaky}, we see that the time constant and synaptic weight of a LI correspond to:
%
\begin{equation*}
\tau = \frac{1}{\lambda_1 - \lambda_2}; \qquad w = \frac{\lambda_1} {\lambda_2}.
\end{equation*}

\subsubsection{Time constant}

$\tau$ determines the distance between the intensities $\lambda_1$ and $\lambda_2$ in the posterior ratio $r$ (see Figure \ref{bigpicture}, upper-left panel).

Passive time constants of neurons are difficult quantities to measure (refs), but as a rule of thumb they vary from about 2 ms (Avian cochlear nucleus) to over 100 ms (CA3 pyramidal cells) (refs).  These time constants correspond to intensity differences of 500 Hz and 10 Hz, respectively.  These intensities are within an order of magnitude of the highest and lowest observed neural spiking rates (refs).  

As a LI's time constant decreases, its response to individual observations of the afferent's spiking output becomes more sensitive (see Figure \ref{bigpicture}, upper-right).  More specifically, the LI's voltage decays faster as no spikes are observed, and jumps higher when a spike is observed.  

This behavior has an intuitive probabilistic interpretation.  A LI with low $\tau$ compares hypotheses that the afferent intensity is very high or very low, e.g.\ whether it is 400 or 40 Hz.  The LI can (and likely will) confidently distinguish between disparate intensities in fewer observations.  Conversely, a LI with high $\tau$ compares hypotheses about similar values of the intensity, e.g.\ whether it is 100 or 110 Hz.  More observations are usually required to confidently distinguish between similar intensities, and the spread of the threshold crossing time increases (Figure \ref{bigpicture}, upper-right).

\begin{figure}[ht!]
\begin{center}
% Use the relevant command to insert your figure file.
% For example, with the graphicx package use
\includegraphics[width=12cm]{figure2.pdf}
\end{center}
% figure caption is below the figure 
\caption{\scriptsize A probabilistic interpretation of $\tau$, $w$, $b$, and $a$.  Upper-left:  LIs with large $\tau$ evaluate the posterior ratio at closely-spaced pairs $\pair$ (marked 1, 2, and 3), and vice versa (marked 4 and 5).  The value of $w$ uniquely determines $\pair$.  Pairs 1, 2, and 3 are given by $\tau = 100$ ms and $w = 20/10, 210/200$, and $410/400$, respectively.  Pairs 4 and 5 are given by $\tau = 2.5$ ms and $w = 440/40$ and $470/70$ respectively.  The black trace represents a hypothetical posterior.  Upper-right:  As $\tau$ increases, the conditional distributions of threshold crossing time $T$ shift higher and spread more.  A LI that distinguishes between disparate intensity values requires a smaller and more predictable number of observations of the afferent's spiking output than a LI that distinguishes between similar intensity values.  Lower-left:  Snapshot of the responses of LIs with $\tau = 100$ ms (white triangles) or $25$ ms (black triangles), but each with different weights.  Each triangle represents the ratio of the posterior at two points and plots that ratio between the points (inset, blow-up of region in the box).  Up-pointing triangles divide the right point by the left point, so $\lambda_1>\lambda_2$, $w>1$, and afferent spikes depolarize the LI (the LI is `excitatory').  Down-pointing triangles divide the left point by the right point, so afferent spikes hyperpolarize the LI (the LI is `inhibitory').  For example, in the inset, $w = 300/290$ (up-pointing arrow) and $290/300$ (down-pointing arrow).  If the posterior is unimodal, these `excitatory' and `inhibitory' voltage traces cross each other near the peak of the posterior, when each has a value of 1.  Excitatory and inhibitory inputs balance near the posterior's peak and are lopsided on its tails.  Lower-right:  A thresholded LI can accept an intensity with an arbitrarily high significance level, but this increases and spreads the conditional distributions of the threshold crossing time.  Thresholded LIs experience both a speed-confidence and a speed-predictability trade-off.}
\label{bigpicture}       % Give a unique label
\end{figure}

\subsubsection{Synaptic weight}

The value of $w$, together with $\tau$, uniquely determines $\lambda_1$ and $\lambda_2$.

Given an array of LIs with identical $\tau$, it is possible to evaluate $r$ at multiple pairs of intensities $\pair$ simultaneously.  This task is accomplished by feeding the afferent spikes into each LI through a particular synaptic weight.  For example, given two LIs with $\tau=100$ms, we can simultaneously evaluate $r$ at $\pair = (20,10)$ and $\pair = (30,20)$ if the LI's weights are $w = 2$ and $w = 1.5$, respectively.

If $\lambda_1 > \lambda_2$, then $w > 1$, and afferent spikes depolarize the LI.  Conversely, if $\lambda_1 < \lambda_2$, then the afferent spikes hyperpolarize the LI and the time constant becomes negative.  

Again, these behaviors have intuitive probabilistic interpretations.  For $\lambda_1 > \lambda_2$, $r$ increases when a spike is observed and decreases as they are not.  The corresponding LI has positive $\tau$ and $w > 1$ accordingly.  Conversely, for $\lambda_1 < \lambda_2$, $r$ decreases when a spike is observed and increases as they are not.  The corresponding LI has negative $\tau$ and $w<1$.

We call LIs with $w > 1$ `excitatory,' and LIs with $w < 1$ `inhibitory.'  If the posterior is unimodal, then LIs that evaluate $r$ at $\pair$ that straddle the peak will have voltages approximately equal to 1 (see Figure \ref{bigpicture}, lower-left).  This is true for both excitatory and inhibitory LIs.  So excitatory and inhibitory LI voltages are balanced and are approximately 1 in regions where the posterior has high probability mass.  As $\pair$ moves away from the peak, the LI's voltages diverge from 1 and either explode or crash (see Figure \ref{bigpicture}, lower-left).  By observing the voltages of many LIs with different synaptic weights, we can estimate not only the location of the posterior's peak, but also its spread.

\subsubsection{Thresholds}

\label{threshold}

Wald's analysis (Section \ref{Wald}) does not require the true afferent intensity $\lambda$ to equal one of the intensity hypotheses $\lambda_1$ or $\lambda_2$.  But if it does, then the LI's thresholds $a$ and $b$ are directly related to the type-I and type-II errors of the SPRT.  

Let $\alpha$ be the probability of accepting $\lambda_1$ when $\lambda_2$ is true, and let $\beta$ be the probability of the converse.  The thresholds are then (ref):
%
\begin{equation*}
a \approx \beta/(1-\alpha); \qquad b \approx (1-\beta)/\alpha,
\end{equation*}
%
where the approximation symbols account for potential overshoots of the barriers (ref).

LIs can accept an intensity at a higher significance level by increasing $b$ and/or decreasing $a$.  However, moving the thresholds further away has two main drawbacks.  The first is that the LI will usually require more observations of the afferent's spiking output to reach either threshold.  The second is that the spread of the threshold crossing time increases (Figure \ref{bigpicture}, lower-right).  The thresholded LI thus experiences both a speed-confidence trade-off and a speed-predictability trade-off (refs).

\section{Discussion}

Implications:
A potential explanation of why the core functional features of neurons have hardly changed for half a billion years:  they already found the Bayes-optimal solution.

Given a neural membrane, what can you infer?  

There are no free parameters.

Other:
Voltage-dependent EPSPs and IPSPs.

Hardware implementation, compare with Deneve.

\section{Appendix}

The probabilities that the LI will accept $\lambda_1$ (left) or $\lambda_2$ (right) are:
%
\begin{equation*}
\Pr(V_T \geq b) \equiv B \approx \frac{1 - e^{ah_0}}{e^{bh_0} - e^{ah_0}}; \qquad \Pr(V_T \leq a) \equiv A = 1 - B,
\end{equation*}
%
where $h_0$ is the unique nonzero value that solves:
%
\begin{equation*}
\E{L_i^{h_0}} = 1, \quad h_0 \neq 0.
\end{equation*}
%
The approximation symbol arises from the possibility that $V_T$ overshoots either threshold (refs).

The characteristic functions of $T$, conditional on hitting either threshold $\psi_b(\tau)$ and $\psi_a(\tau)$, are found by solving two linear equations (ref):
%
\begin{align*}
B \psi_b(\tau) e^{bh_b(\tau)} + A \psi_a(\tau) e^{ah_b(\tau)} &= 1, \\
B \psi_b(\tau) e^{bh_a(\tau)} + A \psi_a(\tau) e^{ah_a(\tau)} &= 1,
\end{align*}
%
where $h_b(\tau)$ and $h_a(\tau)$ are the two (complex) roots of:
%
\begin{equation*}
\label{roots}
\E{L_i^h} - e^{-\tau} = 0,
\end{equation*}
%
for sufficiently small and imaginary values of $\tau$.  The marginal characteristic function of $T$ is then:
%
\begin{equation*}
\psi(\tau) = B \psi_b(\tau) + A \psi_a(\tau).
\end{equation*}

Wald's analysis does not require the afferent's true spiking intensity $\lambda$ to equal $\lambda_1$ or $\lambda_2$.  However, if $\lambda = \lambda_1$ or $\lambda = \lambda_2$, then the thresholds $a$ and $b$ have direct relationships to the type-I and type-II errors of the SPRT (see Section \ref{threshold}).

We can eliminate one of the LI's thresholds by taking the limit $a \to 0$ or $b \to \infty$.  The LI's voltage will eventually hit the remaining threshold if the voltage approaches it on average, e.g.\ we can set $a \to 0$ when $\E{\Pr(S_i|\lambda_1)/\Pr(S_i|\lambda_2)} > 1$, and vice versa.  An LI with one attainable threshold no longer performs a SPRT in the classical sense, but Wald (ref) still obtained the characteristic function of the hitting time for the remaining threshold:
%
\begin{equation*}
\psi_{b \text{ only}}(\tau) = e^{-b h(\tau)},
\end{equation*}
%
where $h(\tau)$ is the root (with negative real part) of Equation \ref{roots}.

\end{document}












