# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 23:27:26 2016

@author: travismonk
"""
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec

dt = .0001

#two sets of points, (l1,l2) and (l1,l3)
l1 = 60; l2 = 50; l3 = 40; l4 = 18

#deterministic spikes.  for plotting purposes only.
S = np.zeros(1000)
S[650] = 1; 

#uniform prior
lam = np.arange(100)
prior = .01 * np.ones(np.shape(lam))

#get the posterior
post = []
post.append(prior)

for i in range(len(S)):
    
    if S[i] == 0:
        num = np.exp(-lam*dt) * prior
        den = np.sum(num)
        #reset prior for next time step
        prior = num/den
        post.append(prior)
        
    if S[i] == 1:
        num = dt*lam*prior
        den = np.sum(num)
        prior = num/den
        post.append(prior)
        
#get traces of voltages for four ratios
V12 = []; V13 = []; V14 = []

for i in range(len(post)):
    V12.append(post[i][l1] / post[i][l2])
    V13.append(post[i][l1] / post[i][l3])
    V14.append(post[i][l1] / post[i][l4])


#plot      
fig = plt.figure(figsize=(12,4))

plt.subplot(121)
gs1 = gridspec.GridSpec(10,1)
gs1.update(left=0.05, right=0.48, wspace=0.05)
ax1 = plt.subplot(gs1[2:,:])



labels = [r'$0$',r'$\lambda_1$',r'$\lambda_2$',r'$\lambda_3$',r'$\lambda_4$',r'$65$']
ax1.plot(post[0][:],color='0.7',linewidth='2')
ax1.plot(post[250][:],'b',color='0.75',linewidth='2')
ax1.plot(post[500][:],'r',color='0.8',linewidth='2')
ax1.plot(post[651][:],'k',color='0',linewidth='2')
ax1.plot(post[850][:],'g',color='0.25',linewidth='2')
ax1.plot(post[-1][:],'b',color='0.40',linewidth='2')
ax1.plot(l1*np.ones(10),np.linspace(0,.05,10),'k--')
ax1.plot(l2*np.ones(10),np.linspace(0,.05,10),'k--')
ax1.plot(l3*np.ones(10),np.linspace(0,.05,10),'k--')
ax1.plot(l4*np.ones(10),np.linspace(0,.05,10),'k--')
plt.text(55.5, .012, '0', fontsize=12,color='k')
ax1.annotate('250', xy=(12, .02), xytext=(12, .012),
            arrowprops=dict(arrowstyle="->",facecolor='black'),)
plt.text(2, .045, '500', fontsize=12,color='k')
ax1.annotate('651', xy=(16, .0241), xytext=(33, .03),
            arrowprops=dict(arrowstyle="->",facecolor='black'),)
ax1.annotate('850', xy=(13, .0313), xytext=(26, .035),
            arrowprops=dict(arrowstyle="->",facecolor='black'),)
ax1.annotate('1000', xy=(11, .0368), xytext=(20, .042),
            arrowprops=dict(arrowstyle="->",facecolor='black'),)
ax1.set_xlim([0,65])
ax1.set_ylim([0,.05])
ax1.set_yticks([0,.05])
ax1.set_yticklabels([r'$0$',r'$0.05$'],fontsize=20)
ax1.set_xticks([0,l1,l2,l3,l4,65])
ax1.set_xticklabels(labels,fontsize=20)
ax1.set_xlabel(r'$\lambda$',fontsize=20)
ax1.xaxis.set_label_coords(1.07, -0.025)
ax1.yaxis.set_label_coords(-.02, .5)
ax1.set_ylabel(r'$\Pr(\lambda|S)$',fontsize=20)


ax2 = plt.subplot(gs1[0,-1])
ax2.plot(S,'k',linewidth='4')
labels = [r'$0$',r'$250$',r'$500$',r'$651$',r'$850$',r'$1000$']
ax2.set_xticks([0,250,500,651,850,999])
ax2.set_xticklabels(labels,fontsize=20)
ax2.set_yticks([])
ax2.set_ylim([0,1.3])
ax2.set_ylabel(r'$S$',fontsize=20,rotation=0)
ax2.yaxis.set_label_coords(-.04, .2)
#ax2.set_xlabel(r'$t$',fontsize=20)
ax2.xaxis.set_label_coords(1.07, -0.035)


plt.subplot(122)
gs2 = gridspec.GridSpec(1,10)
gs2.update(left=0.55, right=0.98, wspace=0.05)
ax3 = plt.subplot(gs2[0,1:-1])


ax3.plot(V12,'k-',linewidth=7,color='.7')
ax3.plot(V13,'k--',linewidth=7,color='.7')
ax3.plot(V14,'k-.',linewidth=7,color='.7')
ax3.plot(V12,'k-',linewidth=2,color='0')
ax3.plot(V13,'k-',linewidth=2,color='0')
ax3.plot(V14,'k-',linewidth=2,color='0')
ax3.set_yticklabels([r'$0$',r'$1$',r'$5$'],fontsize=20)
ax3.set_yticks([0,1])
ax3.set_ylim([0,1.1])
ax3.set_xticks([0,250,500,651,850,999])
ax3.set_xticklabels(labels,fontsize=20)
labels = [r'$\frac{\Pr(\lambda_1|S)}{\Pr(\lambda_2|S)}$',
          r'$\frac{\Pr(\lambda_1|S)}{\Pr(\lambda_3|S)}$',
          r'$\frac{\Pr(\lambda_1|S)}{\Pr(\lambda_4|S)}$',
                   r'$\text{RC}$']
ax3.legend(labels,loc='upper right',fontsize=15,ncol=2,columnspacing=1.,
           handleheight=2,frameon=False)

#ax3.legend(labels,bbox_to_anchor=(1.05, 1), loc=2,
#       ncol=1, mode="expand", borderaxespad=0.)



fig.savefig('figure1.pdf',bbox_inches='tight')