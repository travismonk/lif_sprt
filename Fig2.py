# -*- coding: utf-8 -*-
"""
Created on Sun Apr 24 14:35:52 2016

@author: travismonk
"""
import numpy as np
from scipy.stats import poisson
from matplotlib import pyplot as plt
from matplotlib import gridspec

#time constants
tau = [.1, .0025]

#curly braces
def half_brace(x, beta):
    x0, x1 = x[0], x[-1]
    y = 1/(1.+np.exp(-1*beta*(x-x0))) + 1/(1.+np.exp(-1*beta*(x-x1)))
    return y

dl, x0, xstep = 1./tau[1], 40, .01
xaxis = np.arange(x0, x0+dl/2, xstep)
y0 = half_brace(xaxis, .2)
y1 = np.concatenate((y0, y0[::-1]))

dl, x0, xstep = 1./tau[1], 70, .01
xaxis = np.arange(x0, x0+dl/2, xstep)
y0 = half_brace(xaxis, .2)
y2 = np.concatenate((y0, y0[::-1]))

#get the posterior
t = .01
dt = .0001

#actual intensity
l = 100

mu = float(l)*t

#k is number of observed spikes
#k = poisson.rvs(mu=mu)
k = 1

#uniform prior
lam = np.arange(500)
prior = np.ones(np.shape(lam)); prior = prior / np.sum(prior)

#posterior after some amount of time
post = poisson.pmf(k=k,mu=lam*t)*prior; post[0] = 0; post = post / np.sum(post)

#low, middle, high weights
ll1 = [10,10+1/tau[0]]; lm1 = [200,200+1/tau[0]]; lu1 = [400,400+1/tau[0]]
ll2 = [40,40+1/tau[1]]; lu2 = [70,70+1/tau[1]]; 

fig = plt.figure(1,figsize=[10,5])

gs1 = gridspec.GridSpec(20,20)
gs1.update(left=0.08, right=0.98, wspace=0.05)

ax1 = plt.subplot(gs1[0:8,0:12])

ax1.plot(lam,post,'k',linewidth=2)
ax1.plot(ll1[0]*np.ones(10),np.linspace(0,.8*np.max(post),10),'k--')
ax1.plot(ll1[1]*np.ones(10),np.linspace(0,.8*np.max(post),10),'k--')
ax1.plot(lm1[0]*np.ones(10),np.linspace(0,.8*np.max(post),10),'k--')
ax1.plot(lm1[1]*np.ones(10),np.linspace(0,.8*np.max(post),10),'k--')
ax1.plot(lu1[0]*np.ones(10),np.linspace(0,.8*np.max(post),10),'k--')
ax1.plot(lu1[1]*np.ones(10),np.linspace(0,.8*np.max(post),10),'k--')
ax1.plot(ll2[0]*np.ones(10),np.linspace(0,np.max(post),10),'k--')
ax1.plot(ll2[1]*np.ones(10),np.linspace(0,np.max(post),10),'k--')
ax1.plot(lu2[0]*np.ones(10),np.linspace(0,np.max(post),10),'k--')
ax1.plot(lu2[1]*np.ones(10),np.linspace(0,np.max(post),10),'k--')

#curly braces manipulation -- shift and scale
y1 = y1 - np.min(y1); y1 = .15 * np.max(post) * y1; y1 = y1 + 1.1* np.max(post)
y2 = y2 - np.min(y2); y2 = .15 * np.max(post) * y2; y2 = y2 + 1.45* np.max(post)
ax1.set_ylim([0,2*np.max(post)])
ax1.set_xlim([0,501])
ax1.set_yticklabels([r'$0$',r'$0.004$'],fontsize=20)
ax1.set_xticklabels([r'$0$',r'$500$'],fontsize=20)
ax1.set_yticks([0,.004])
ax1.set_xticks([0,500])
ax1.set_xlabel(r'$\lambda$',fontsize=20)
ax1.xaxis.set_label_coords(.5, -0.035)


ax1.plot(np.arange(40,40+1./tau[1],xstep), y1, 'k')
ax1.plot(np.arange(70,70+1./tau[1],xstep), y2, 'k')
plt.text(13, .0033, '1', fontsize=16,color='k')
plt.text(202, .0033, '2', fontsize=16,color='k')
plt.text(402, .0033, '3', fontsize=16,color='k')
plt.text(240, .0049, '4', fontsize=16,color='k')
plt.text(270, .0065, '5', fontsize=16,color='k')

ax2 = plt.subplot(gs1[10:-1,0:12])
ax2.set_ylim([0,.004])

exc = [post[i]/post[i-10] for i in np.arange(10,500,10)]; exc[0] = 1e6
inh = [post[i]/post[i+10] for i in np.arange(0,490,10)];

exc2 = [post[i]/post[i-40] for i in np.arange(40,500,20)]; exc2[0] = 1e6
inh2 = [post[i]/post[i+40] for i in np.arange(0,460,20)];

ax2.plot(lam,post,'k',linewidth=2)
ax2.set_yticklabels([r'$0$',r'$0.004$'],fontsize=20)
ax2.set_xticklabels([r'$0$',r'$500$'],fontsize=20)
ax2.set_yticks([0,.004])
ax2.set_ylim([0,.005])
ax2.set_xlim([0,500])
ax2.set_xticks([0,499])
ax2.set_xlabel(r'$\lambda$',fontsize=20)
ax2.xaxis.set_label_coords(.5, -0.035)


ax3 = ax2.twinx()
ax3.plot(np.arange(5,495,10),exc,'-k^',MarkerFaceColor='w')
ax3.plot(np.arange(5,495,10),inh,'-kv',MarkerFaceColor='w')
ax3.plot(np.arange(20,480,20),exc2,'-k^',MarkerFaceColor='k')
ax3.plot(np.arange(20,480,20),inh2,'-kv',MarkerFaceColor='k')
ax3.set_ylim([.4,2.25])
ax3.set_yticklabels([r'$.5$',r'$1$',r'$2$'],fontsize=20)
ax3.set_yticks([.5,1,2])


a = plt.axes([.37, .379, .18, .1], axisbg='w')
a.plot(np.arange(287,303,1),post[287:303],'k')
a2 = a.twinx()
a2.plot(295,post[300]/post[290],'w^')
a2.plot(295,post[290]/post[300],'wv')
a2.set_xticks([290,295,300])
a.set_yticks([])
a.set_xlim([289,301])
a2.set_yticks([])
a.plot(290,post[290],'ko')
a.plot(300,post[300],'ko')
a2.set_ylim([.9,1.12])

ax3.plot(np.arange(280,305,1),1.25*np.ones(len(np.arange(280,305,1))),'k-')
ax3.plot(np.arange(280,305,1),.8*np.ones(len(np.arange(280,305,1))),'k-')
ax3.plot(280*np.ones(len(np.arange(.8,1.25,.01))),np.arange(.8,1.25,.01),'k-')
ax3.plot(305*np.ones(len(np.arange(.8,1.25,.01))),np.arange(.8,1.25,.01),'k-')
ax3.plot(lam,np.ones(len(lam)),'k--')
ax3.plot(100*np.ones(len(lam)),lam,'k--')

#np.savez('walddists.npz',tc=tc,w=w,dt=dt,a=a,b=b,lam=lam,
#dists1=dists1,dists2=dists2,prob=prob)
with np.load('walddists.npz') as data:
    dists1 = data['dists1']; dists2 = data['dists2']

ax4 = plt.subplot(gs1[0:8,13:-1])
ax4.plot(dists1[0],'k:',label=r'$\tau=50 ms$')
ax4.plot(dists2[0],'k:')
ax4.set_xticks([0,3000])
ax4.set_xlim([0,3000])
ax4.plot(dists1[1],'k--',label=r'$\tau=75 ms$')
ax4.plot(dists2[1],'k--')
ax4.plot(dists1[2],'k-',label=r'$\tau=100 ms$')
ax4.plot(dists2[2],'k-')
ax4.set_xticklabels([r'$0$',r'$3000$'],fontsize=20)
ax4.set_yticks([])
legend = ax4.legend(labelspacing=.1,frameon=False)
for label in legend.get_texts():
    label.set_fontsize('large')
ax4.set_xlabel(r'$T$',fontsize=20)
ax4.xaxis.set_label_coords(.5, -0.035)


#np.savez('waldthresh.npz',tc=tc,w=w,dt=dt,err=err,lam=lam,
#dists1=dists1,dists2=dists2,prob=prob)
with np.load('waldthresh.npz') as data:
    distst1 = data['dists1']; distst2 = data['dists2']

ax5 = plt.subplot(gs1[10:-1,13:-1])
ax5.plot(distst1[0][np.arange(0,80000,1000)],'k:',label=r'$\alpha=\beta=.1$')
ax5.plot(distst2[0][np.arange(0,80000,1000)],'k:')
ax5.plot(distst1[1][np.arange(0,80000,1000)],'k--',label=r'$\alpha=\beta=.01$')
ax5.plot(distst2[1][np.arange(0,80000,1000)],'k--')
ax5.plot(distst1[2][np.arange(0,80000,1000)],'k-',label=r'$\alpha=\beta=.001$')
ax5.plot(distst2[2][np.arange(0,80000,1000)],'k-')
ax5.set_xlim([0,40])
ax5.set_xticks([0,40])
ax5.set_xticklabels([r'$0$',r'$40000$'],fontsize=20)
ax5.set_yticks([])
legend = ax5.legend(labelspacing=.1,frameon=False)
for label in legend.get_texts():
    label.set_fontsize('large')
ax5.set_xlabel(r'$T$',fontsize=20)
ax5.xaxis.set_label_coords(.5, -0.035)


fig.savefig('figure2.pdf',bbox_inches='tight')






