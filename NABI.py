'''
Python code that performs real-time, instantaneous inference for the rate of a Poisson process
'''
from pylab import *
from mpl_toolkits.mplot3d import Axes3D

#real-time inference for one spiking input
dt = 1e-3
num = 100
dur = 10000
rate = linspace(10,80,num)
a = exp(-rate*dt)
v = ones((dur+1,num))/num

in_rate = 60

for t in range(dur):

    if rand()<=in_rate*dt: # input spike 
        v[t] = v[t]*(1-a)  
    
    v[t+1] = v[t]*a # standard LPF decay
    v[t+1] /= sum(v[t+1]) # normalise v[t+1]

# 3D plot of posterior density vs time
close(1)
ax1 = figure(1).add_subplot(211, projection='3d')
X,Y = meshgrid(rate,linspace(0,dur,dur+1)*dt)
ax1.plot_surface(X,Y,v, rstride=10, cstride=10, cmap=cm.jet, linewidth=0)
ax1.set_xlabel("Rate (spikes/s)")
ax1.set_ylabel("Time (s)")
ax1.set_zlabel("p(rate|data(0:t))")
_ = ax1.set_title("Posterior Density")

print "Actual rate = %0.2f; Estimated rate = %0.2f" % (in_rate, rate[v[t+1].argmax()])

#real-time inference for many spiking inputs.
#N inputs with same rate lambda.  
N = 2
dur = 10000
vN = ones((dur+1,num))/num
rate = linspace(10,300,num)

for t in range(dur):
    
    draw = rand(N); spikegen = [in_rate*dt for i in range(N)]
    #how many spikes on a time step?
    spikes = sum(draw <= spikegen)
    
    vN[t] = vN[t] * (1-a)**(spikes)
    vN[t+1] = vN[t] * a**N

    vN[t+1] /= sum(vN[t+1])
    
ax2 = figure(1).add_subplot(212, projection='3d')
X,Y = meshgrid(rate,linspace(0,dur,dur+1)*dt)
ax2.plot_surface(X,Y,vN, rstride=10, cstride=10, cmap=cm.jet, linewidth=0)
ax2.set_xlabel("Rate (spikes/s)")
ax2.set_ylabel("Time (s)")
ax2.set_zlabel("p(rate|data(0:t))")
_ = ax2.set_title("Posterior Density")   

show() 




