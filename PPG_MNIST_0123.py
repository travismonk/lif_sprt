# -*- coding: utf-8 -*-
"""
Created on Thu May 28 20:34:16 2015

@author: travismonk
"""

'''
This script trains the Poisson-Gamma neural network on MNIST.

Running this script produces two files: weightandlam.npz and weightnolam.npz 
These files contain snapshots of the weights for all hidden units after each
iteration over the training data.  So we can watch the weights evolve over
successive iterations.

These files also contain snapshots of intrinsic parameters for all hidden 
units.

These files are imported in other scripts, testerror.py and plotter.py, in
evaluating test error and plotting results.

To run over all MNIST or subset of it, change "digits" appropriately.
'''

import numpy as np
from scipy.stats import poisson

from mnist import load_mnist

def SoftMax(I):
    # softmax-function with overflow fix
    # over/underflow in np.exp(x) for approximately x > 700 or x < -740
    scale = 0.
    if (I[np.argmax(I)] > 700):
        scale  = I[np.argmax(I)] - 700
    if (I[np.argmin(I)] < -740 + scale):
        I[np.argmin(I)] = -740 + scale
    return np.exp(I-scale) / np.sum(np.exp(I-scale))

file_lam = 'WandL_0123_L0mu_v3.npz'; file_nolam = 'WnoL_0123_L0mu_v3.npz'

#load MNIST as np array
dataset = "training"; digits = np.arange(4); path = "./Data/MNIST/"
images,labels = load_mnist(dataset,digits,path)

#dimensionality of data point.  number of input units is D**2                   
D = len(images[0][0]); h = len(digits)**2

#learning rule for weights and lambda, size of training set, runs over training
train = len(images); runs = 100; eps = .0001 #eps = .01*h/train

#offset zeros in the inputs; background = 1, limit range from 0 to 1
images = [(images[i]+1.)*1./256 for i in range(len(images))]

#to run network without lambda dynamcs, first need to run it with lambda.
#this gives us final values of lambda, which we use to initialize the network
#that has no lambda dynamics.
for lambda_dynamics in range(2):

    #initialize the weights as poisson-distributed
    #sum all inputs and average for each pixel
    weightnormer = np.sum(images,axis=0)/len(images)
    W = [(poisson.rvs(weightnormer)+1.)/(D**2) for i in range(h)]
    
    #sum all the inputs and average for each image
    lamnormer = np.sum(weightnormer)
    lam = np.array([lamnormer for i in range(h)])
       
    Wplot = [W]; lamplot = [lam]
    
    for r in range(runs):
        
        print r+1, " runs out of ", runs
        
        for n in range(train):
        
            #sum of inputs
            yhat = np.sum(images[n]);
        
            #intensity
            I = [np.sum(images[n]*np.log(lam[c]*W[c])) - lam[c] for c in range(h)]
            
            #activity for the hidden layer is the posterior
            sc = SoftMax(np.array(I))
            
            #sum of the weights
            what = [np.sum(W[c]) for c in range(h)]
            
            #update the weights
            nw = [eps*sc[c]*(images[n]-lam[c]*what[c]*W[c])+W[c] for c in range(h)]
            
            if lambda_dynamics == 0:        
                #update mean of the gamma distributions
                newlam = [eps*sc[c]*(yhat-lam[c])+lam[c] for c in range(h)]
            else:
                newlam = lam
            
            #update parameters (you idiot).
            lam = newlam; W = nw        
            
            #store every datapt^th data point.
            if round(float(n)/train) == float(n)/train:
                print n, " data points out of ", train
                Wplot.append(W);  lamplot.append(lam)
    
    #save results to file
    if lambda_dynamics == 0:
        file = open(file_lam,'w+')
        np.savez(file_lam,W=Wplot,lam=lamplot,eps=eps,h=h,\
                images=images,labels=labels,digits=digits)
    elif lambda_dynamics == 1:
        file = open(file_nolam,'w+')
        np.savez(file_nolam,W=Wplot,lam=lamplot,eps=eps,h=h,\
                images=images,labels=labels,digits=digits)

del images, labels

