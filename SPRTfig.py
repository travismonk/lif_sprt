# -*- coding: utf-8 -*-
"""
Created on Thu May 26 23:44:36 2016

@author: travismonk
"""

#SPRT figure
import numpy as np
from matplotlib import pyplot as plt

#pdf1=pdf1,pdf2=pdf2,ta=ta,tb=tb

with np.load('waldanalysisfig.npz') as data:
    pdf1 = data['pdf1']; pdf2 = data['pdf2']; ta = data['ta']; tb = data['tb']
    alpha = data['alpha']; a = data['a']; b = data['b'];
    lam1 = data['lam1']; lam2 = data['lam2']; lam = data['lam']; dt = data['dt']

#a = np.log(1.5); b = np.log(.3)

ae = np.exp(a); be = np.exp(b); #barriers

#by stochastic simulation
V = []; trials = 8

for i in range(trials):
    print i
    Sn = []; Sn.append(1)   #corresponds to uniform prior.
    while be < Sn[-1] and Sn[-1] < ae:
        
        #observe a spike or not
        S = (np.random.rand(1)[0] < lam * dt)
        
        #get log likelihood
        L = (lam1/lam2)**S * np.exp(-(lam1 - lam2) * dt)
        Sn.append(Sn[-1] * L);
    V.append(np.array(Sn))

#the rightside-up histogram
plt.figure(1,figsize=(12,6))
plt.hist(ta, bins=180, range=(0,4500), color = 'k', alpha = .3, weights=.0001*np.ones(np.shape(ta)), bottom = a)
plt.xlim([0,3000])
plt.xticks([0,3000])
labels = ['a','b']
plt.yticks([.1422,.4054],labels); 
plt.tick_params(axis='both',labelsize=16)
plt.ylim([0,.74])
plt.plot(135*pdf1+a,'k',linewidth=3)
T = np.arange(0,3000,100)
plt.plot(T,.4054*np.ones(np.shape(T)),'k--',linewidth=3)
plt.plot(T,.147*np.ones(np.shape(T)),'k--',linewidth=3)

#the LI traces
for i in range(trials):
    plt.plot(np.log(V[i])*(.385-.225) + .225 + .7*(.385-.225),'k')
    plt.plot(len(V[i]),np.log(V[i][-1])*(.385-.225) + .225 + .7*(.385-.225),'ko')

#the upside-down histogram
ax2 = plt.twinx()
plt.hist(tb, bins=180, range=(250,4500), color = 'k', alpha = .3, weights=.00022*np.ones(np.shape(tb)),bottom=-b)
plt.plot(225*pdf2-b,'k',linewidth=3)
plt.xlim([0,3000])
plt.ylim([0,1.5])
plt.gca().invert_yaxis()
plt.yticks([])

#the marginal inset
t = np.concatenate((ta,tb),axis=0)
pdf = alpha*pdf1 + (1-alpha)*pdf2
a = plt.axes([.35, .65, .5, .2], axisbg='w')
n, bins, patches = plt.hist(t, bins=68, range=(1,3200),normed=1,color='k',alpha=.3)
plt.plot(pdf,'k',linewidth=3)
plt.plot(pdf1,'k--',linewidth=1)
plt.plot(pdf2,'k--',linewidth=1)
plt.xlim([0,2000])
plt.ylim([0,.0016])
plt.xticks([0,2000])
plt.tick_params(axis='both',labelsize=16)
plt.yticks([])

#the absorption probabilities
b = plt.axes([.58, .31, .3, .15], axisbg='w')
labels = ['B','A']
plt.xticks([.25,1.25],labels)
plt.yticks([0,1])
plt.bar(0,alpha,.25,color='0',label='Theory')
plt.bar(0+.25,float(len(ta))/len(t),.25,color='1',label='Simulation')
plt.bar(1,1-alpha,.25,color='0')
plt.bar(1+.25,float(len(tb))/len(t),.25,color='1')
plt.tick_params(axis='both',labelsize=16)
plt.xlim([-.25,1.75])
plt.ylim([0,1])
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.)

plt.savefig('SPRT.pdf',bbox_inches='tight')