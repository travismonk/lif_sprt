# -*- coding: utf-8 -*-
"""
Created on Sun May 22 14:11:18 2016

@author: travismonk
"""

import numpy as np
import scipy as sp
from scipy.optimize import fsolve, minimize, newton
from matplotlib import pyplot as plt

#Sequential probability ratio test

with np.load('waldsimfig.npz') as data:
    lam1 = data['lam1']; lam2 = data['lam2']; lam = data['lam']; dt = data['dt']; 
    a = np.log(data['a']); b = np.log(data['b']); ta = data['ta']; tb = data['tb']
 
    
#by Wald's analysis
#MGF of a step
def root(h,tau):
    MGFm1 = (lam1/lam2)**h * np.exp(-(lam1-lam2)*dt*h)*(1-np.exp(-lam*dt)) + \
        np.exp(-(lam1-lam2)*dt*h)*np.exp(-lam*dt) - np.exp(-tau)
    return MGFm1

def deriv(h,tau):
    ddh = (1-np.exp(-lam*dt))*(np.exp(-(lam1-lam2)*dt*h)*(lam1/lam2)**h * \
    (-(lam1-lam2)*dt + np.log(lam1/lam2))) + \
    np.exp(-lam*dt)*np.exp(-(lam1-lam2)*dt*h)*(-(lam1-lam2)*dt)
    return ddh

#get h0
h0 = fsolve(root, 10., args=(0.))

#probability of absorption at a
alpha = (1 - np.exp(b*h0)) / (np.exp(a*h0) - np.exp(b*h0))
print alpha

#now get the roots of the MGF as a function of tau.
#first the minimum (helps set initial rootfinder)
bot = minimize(root, 1, args=(0,))['x']
switchtau = root(bot,0)

#get the roots as a function of tau
am = .1j; dtau = 1e-4;
tau = sp.linspace(-am,am,1./dtau)

#real variable for plotting
tp = np.real(-tau*1j)

t1 = []; t2 = []; cf1 = []; cf2 = []
init = [400-1j,-400-1j]
for i in tau:
    
    t1.append(newton(root,init[0],args=(i,),fprime=deriv,maxiter=100000,tol=1e-7))
    t2.append(newton(root,init[1],args=(i,),fprime=deriv,maxiter=100000,tol=1e-7))    
    init = [t1[-1], t2[-1]]
 
t2 = np.array(t2); t1 = np.array(t1)  

#now write down the characteristic functions
cf2 = (1 - np.exp(a*(t2-t1))) * (1./(np.exp(a*(t2-t1))*(alpha-1)*np.exp(b*t1) +\
     (1-alpha)*np.exp(b*t2)))

cf1 = (1+(alpha-1)*cf2*np.exp(b*t1)) / (alpha * np.exp(a*t1))
  
#inverse-transform to get conditional pdf of absorption times    
def cdf2pdf(n,cf):
    pdf = (np.max(tp) - np.min(tp)) * \
            (1./(2*np.pi)) * np.sum(np.exp(-1j*tp*n) * cf) * dtau
    return pdf
 
print t1[-1], t2[-1]


pdf2 = []; pdf1 = [];

for i in range(5000):
    print i
    pdf1.append(cdf2pdf(i,cf1))
    pdf2.append(cdf2pdf(i,cf2))
    



plt.figure(1)
plt.subplot(321)
plt.hist(ta, bins=600, color = 'k', alpha = .3, normed=1)
plt.hist(tb, bins=600, color = 'r', alpha = .3, normed=1)
plt.plot(pdf2,'r')
plt.plot(pdf1,'k')
plt.xlim([0,i])

plt.subplot(322)
h = np.arange(-.5,2,.01)
plt.plot(h,root(h,0),'k')

plt.subplot(323)
plt.plot(tp,np.real(t2),'r',linewidth=2)
plt.plot(tp,np.imag(t2),'r--')

plt.subplot(324)
plt.plot(tp,np.real(cf2),'r')
plt.plot(tp,np.imag(cf2),'r--')

plt.subplot(325)
plt.plot(tp,np.real(t1),'k',linewidth=2)
plt.plot(tp,np.imag(t1),'k--')

plt.subplot(326)
plt.plot(tp,np.real(cf1),'k')
plt.plot(tp,np.imag(cf1),'k--')

file = open('waldanalysisfig.npz','w+')
np.savez('waldanalysisfig.npz',pdf1=pdf1,pdf2=pdf2,ta=ta,tb=tb,alpha=alpha,\
        a=a,b=b,lam1=lam1,lam2=lam2,lam=lam,dt=dt)