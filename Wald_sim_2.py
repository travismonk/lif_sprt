# -*- coding: utf-8 -*-
"""
Created on Fri May 13 10:18:52 2016

@author: travismonk
"""
import numpy as np

#Sequential probability ratio test
#Working in ratio space.  Analysis in log space.  Shouldn't matter.

lam1 = 45.; lam2 = 40.; lam = lam2; dt = .001

a = 1.5; b = .3; #barriers

#by stochastic simulation
ta= []; tb = []; trials = 100000

for i in range(trials):
    Sn = 1   #corresponds to uniform prior.  in log space, prior ratio is 0.
    t = 0
    print i
    while b < Sn and Sn < a:
        
        #observe a spike or not
        S = (np.random.rand(1)[0] < lam * dt)
        
        #get log likelihood
        L = (lam1/lam2)**S * np.exp(-(lam1 - lam2) * dt)
        Sn = Sn * L;  t = t + 1; 
        
    if Sn < b:
        tb.append(t)
    if Sn > a:
        ta.append(t)

file = open('waldsimfig.npz','w+')
np.savez('waldsimfig.npz',ta=ta,tb=tb,lam=lam,lam1=lam1,lam2=lam2,dt=dt,a=a,b=b)