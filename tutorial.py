# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 17:28:02 2016

@author: travismonk
"""

import numpy as np
import os

#this is the directory where the MNIST database is stored on my laptop.
os.chdir('/Users/travismonk/Documents/Repos/poissongamma/MNIST-0123/Data/MNIST')

#show the current working directory
os.getcwd()